﻿using MMDB.MovieDBService;
using System;
using System.Collections.Generic;

namespace MMDB
{
    class Program : MMDBConsoleApp
    {
        private MovieServiceClient movieService;
        private CastOrCrewServiceClient castOrCrewService;
        private SearchServiceClient searchService;

        static void Main(string[] args)
        {
            new Program();
        }

        protected override void Initialize()
        {
            movieService = new MovieServiceClient();
            castOrCrewService = new CastOrCrewServiceClient();
            searchService = new SearchServiceClient();

            AddCommand('1', "Search movie", FindMovie);
            AddCommand('2', "Search actor/director", FindActorOrDirector);
            AddCommand('3', "Free text search", FreeTextSearch);
            AddCommand('0', "Quit", Quit, false);
        }

        private void FindActorOrDirector()
        {
            string name;
            AskFor(out name, "Enter the name of the actor or direcotr you're searching for: ");
            var castOrCrew = castOrCrewService.FindCastOrCrewByName(name);
            if (castOrCrew == null)
            {
                Console.WriteLine($"Cannot find {name}");
            }
            else
            {
                PrintCastOrCrew(castOrCrew);
            }
        }

        private void FindMovie()
        {
            string title;
            AskFor(out title, "Enter the title of the movie you are searching for: ");

            var movie = movieService.FindMovieByTitle(title);
            if (movie == null)
            {
                Console.WriteLine($"Cannot find a movie called {title}");
            }
            else
            {
                PrintMovie(movie);
            }
        }

        protected override void FreeTextSearch()
        {
            string searchText;
            AskFor(out searchText, "Enter the search text: ");

            var searchResults = searchService.Search(searchText, true);
            Console.WriteLine("Search result:");
            Console.WriteLine("--------------");
            
            foreach (var searchResultItem in searchResults)
            {
                Console.WriteLine(searchResultItem);
            }
        }
        private void PrintMovie(Movie movie)
        {
            Console.WriteLine($"{movie.Title} ({movie.ProductionYear.Value})");
            Console.Write("Directed by: ");
            bool commaNeeded = false;
            var diretors = movieService.GetMovieDirectors(movie);
            foreach (var director in diretors)
            {
                Console.Write($"{(commaNeeded ? ", " : "")}{director.Name}");
                commaNeeded = true;
            }
            Console.WriteLine();

            Console.WriteLine("Actors");
            Console.WriteLine("------");
            var actors = movieService.GetMovieActors(movie);
            foreach (var actor in actors)
            {
                Console.WriteLine($"{actor.Name}");
            }
        }

        private void PrintCastOrCrew(CastOrCrew castOrCrew)
        {
            Console.WriteLine($"{castOrCrew.Name}");
            Console.WriteLine($"{castOrCrewService.GetJobTitle(castOrCrew)}");
            Console.WriteLine($"Born {castOrCrew.DateOfBirth:yyyy-MM-dd}");
            if (castOrCrewService.IsDirector(castOrCrew))
            {
                Console.WriteLine();
                Console.WriteLine("Director for the following movies:");
                Console.WriteLine("----------------------------------");
                IEnumerable<Movie> movies = castOrCrewService.GetDirectedMovies(castOrCrew);
                foreach (var movie in movies)
                {
                    Console.WriteLine($"{movie.Title}");
                }
            }

            if (castOrCrewService.IsActor(castOrCrew))
            {
                Console.WriteLine();
                Console.WriteLine("Actor in the ollowing movies:");
                Console.WriteLine("------------------------------");
                IEnumerable<Movie> movies = castOrCrewService.GetActedMovies(castOrCrew);
                foreach (var movie in movies)
                {
                    Console.WriteLine($"{movie.Title}");
                }
                Console.WriteLine();
                Console.WriteLine("Co-actors:");
                Console.WriteLine("----------");
                var coactorDictionary = castOrCrewService.GetCoActors(castOrCrew);

                foreach (var coactor in coactorDictionary.Keys)
                {
                    Console.Write($"{coactor.Name}: ");
                    var coactedMovies = coactorDictionary[coactor];
                    bool commaNeeded = false;
                    foreach (var movie in coactedMovies)
                    {
                        if (commaNeeded)
                        {
                            Console.Write(", ");
                        }
                        else
                        {
                            commaNeeded = true;
                        }

                        Console.Write($"{movie.Title} ({movie.ProductionYear.Value})");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
