﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUtil
{
    struct ConsoleCommand
    {
        public char command;
        public string label;
        public bool printHeader;
        public Action action;

        public ConsoleCommand(char command, string label, Action action, bool printHeader)
        {
            this.command = command;
            this.label = label;
            this.action = action;
            this.printHeader = printHeader;
        }

        public ConsoleCommand(char command, string label, Action action)
            : this(command, label, action, true)
        {

        }
    }

    public abstract class ConsoleApp
    {
        bool keepRunning;
        Dictionary<char, ConsoleCommand> commandMapper;
        private const int HEADER_MAX_WIDTH = 50;

        protected ConsoleApp()
        {
            commandMapper = new Dictionary<char, ConsoleCommand>();
            Initialize();
            RunLoop();
        }

        protected abstract void Initialize();
        protected void RunLoop()
        {
            keepRunning = true;
            do
            {
                PrintMenu();
                var command = GetCommand();
                ExecuteCommand(command);
            } while (keepRunning);
        }
        protected void EndLoop()
        {
            keepRunning = false;
        }
        protected virtual void Quit()
        {
            bool quit;
            AskFor(out quit, "Do you really want to quit?");
            if (quit)
            {
                EndLoop();
            }
        }
        protected char GetCommand()
        {
            var command = Console.ReadKey().KeyChar;
            Console.WriteLine();
            return command;
        }

        protected void ExecuteCommand(char command)
        {
            if (commandMapper.ContainsKey(command))
            {
                var label = commandMapper[command].label;
                var printHeader = commandMapper[command].printHeader;
                if (printHeader)
                {
                    Clear();
                    PrintHeader(label);
                }
                var action = commandMapper[command].action;
                action();
            }
            else
            {
                Console.WriteLine($"Unknown command '{command}'!");
            }
        }
        protected void AddCommand(char command, string commandLabel, Action commandAction)
        {
            commandMapper[command] = new ConsoleCommand(command, commandLabel, commandAction);
        }

        protected void AddCommand(char command, string commandLabel, Action commandAction, bool printHeader)
        {
            commandMapper[command] = new ConsoleCommand(command, commandLabel, commandAction, printHeader);
        }

        protected void PrintHeader(string label)
        {
            int leftMargin = (HEADER_MAX_WIDTH - label.Length) / 2;
            leftMargin = (leftMargin <= 0) ? 1 : leftMargin;
            int rightMargin = (HEADER_MAX_WIDTH % label.Length == 0) ? leftMargin : leftMargin + 1;
            rightMargin = (rightMargin <= 0) ? 1 : rightMargin;
            int rowLength = label.Length + leftMargin + rightMargin;

            StringBuilder barBuilder = new StringBuilder();
            barBuilder.Append("+").Append('-', rowLength).Append("+");
            StringBuilder labelBuilder = new StringBuilder();
            labelBuilder.Append("|").Append(' ', leftMargin).Append(label).Append(' ', rightMargin).Append("|");

            string barString = barBuilder.ToString();
            string labelString = labelBuilder.ToString();
            Console.WriteLine(barString);
            Console.WriteLine(labelString);
            Console.WriteLine(barString);
            Console.WriteLine();
        }

        protected void PrintMenu()
        {
            StringBuilder menuBuilder = new StringBuilder();
            foreach (var command in commandMapper.Keys)
            {
                var label = commandMapper[command].label;
                menuBuilder.Append($"{command}) {label}  ");
            }
            PrintHeader(menuBuilder.ToString());
            Console.Write("Enter one of the above commands: ");
        }

        protected void AskFor(out int answer, string prompt)
        {
            Console.Write(prompt);
            var line = Console.ReadLine();
            while (!int.TryParse(line, out answer))
            {
                Console.Write("Please enter a numeric value: ");
                line = Console.ReadLine();
            }
        }

        protected void AskFor(out double answer, string prompt)
        {
            Console.Write(prompt);
            var line = Console.ReadLine();
            while (!double.TryParse(line, out answer))
            {
                Console.Write("Please enter a decimal value: ");
                line = Console.ReadLine();
            }
        }

        protected void AskFor(out DateTime answer, string prompt)
        {
            Console.Write(prompt);
            var line = Console.ReadLine();
            while (!DateTime.TryParse(line, out answer))
            {
                Console.Write("Please enter a date value: ");
                line = Console.ReadLine();
            }
        }

        protected void AskFor(out string answer, string prompt)
        {
            Console.Write(prompt);
            answer = Console.ReadLine();
        }

        protected void AskFor(out bool answer, string prompt)
        {
            Console.Write($"{prompt} (y/n) ");
            var keyChar = Char.ToLower(Console.ReadKey().KeyChar);
            Console.WriteLine();
            while (keyChar != 'y' && keyChar != 'n')
            {
                Console.Write("Please answer y for yes or n for no: ");
                keyChar = char.ToLower(Console.ReadKey().KeyChar);
                Console.WriteLine();
            }
            answer = keyChar == 'y';
        }

        protected void Clear()
        {
            Console.Clear();
        }
    }
}
