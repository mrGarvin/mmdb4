﻿using ConsoleUtil;
using System;
using MMDB.MovieDBService;

namespace MMDB
{
    public abstract class MMDBConsoleApp : ConsoleApp
    {
        protected abstract void FreeTextSearch();

        public void AskFor(out ProductionYear answer, string prompt)
        {
            Console.Write(prompt);
            var productionYear = new ProductionYear();
            bool validYear = false;
            while (!validYear)
            {
                var line = Console.ReadLine();
                int year;
                if (int.TryParse(line, out year))
                {
                    productionYear.Value = year;
                    ValidatorClient validatorClient = new ValidatorClient();
                    validYear = validatorClient.ValidateProductionYear(productionYear);
                }

                if (!validYear)
                {
                    Console.Write("Please enter a valid production year: ");
                }
            }
            answer = productionYear;
        }
    }
}
