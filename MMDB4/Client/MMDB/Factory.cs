﻿using System;
using MMDB.MovieDBService;

namespace MMDB
{
    internal class Factory
    {
        internal static CastOrCrew CreateCastOrCrew(string name, DateTime dateOfBirth)
        {
            var castOrCrew = new CastOrCrew();
            castOrCrew.Id = Guid.NewGuid();
            castOrCrew.Name = name;
            castOrCrew.DateOfBirth = dateOfBirth;
            return castOrCrew;
        }
        internal static Movie CreateMovie(string title, ProductionYear productionYear)
        {
            var movie = new Movie();
            movie.Id = Guid.NewGuid();
            movie.Title = title;
            movie.ProductionYear = productionYear;
            return movie;
        }
    }
}