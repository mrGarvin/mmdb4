﻿using MMDBService.MovieDatabase.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MMDBHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(MovieDatabaseService));
            host.Open();
            Console.WriteLine("Host is running...Press any key to exit");
            Console.ReadKey();
        }
    }
}
