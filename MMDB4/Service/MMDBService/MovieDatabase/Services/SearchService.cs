﻿using MMDB.MovieDatabase.Repositories;
using System.Collections.Generic;
using System.Linq;
using MMDB.MovieDatabase.Helper;
using MMDB.MovieDatabase.Domain.ValueObjects;
using MMDBService.MovieDatabase.Interfaces;
using System;

namespace MMDB.MovieDatabase.Services
{
    public class FreeTextSearchService : ISearchService
    {
        public string[] Search(string freeText, bool ignoreCase)
        {
            throw new NotImplementedException();
        }

        private MovieRepository MovieRepo
        {
            get { return MovieRepository.Instance; }
        }

        private CastOrCrewRepository CastOrCrewRepo
        {
            get { return CastOrCrewRepository.Instance; }
        }
    }
}
