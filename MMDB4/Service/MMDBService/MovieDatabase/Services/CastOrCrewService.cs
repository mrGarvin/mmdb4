﻿using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Repositories;
using MMDBService.MovieDatabase.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMDB.MovieDatabase.Services
{
    class CastOrCrewService : ICastOrCrewService
    {
        public void AddCastOrCrew(CastOrCrew castOrCrew)
        {
            Repository.Add(castOrCrew);
        }

        public CastOrCrew FindCastOrCrewById(Guid id)
        {
            return Repository.FindBy(id);
        }

        public CastOrCrew FindCastOrCrewByName(string name)
        {
            return Repository.FindBy(name);
        }

        public IEnumerable<Movie> GetActedMovies(CastOrCrew castOrCrew)
        {
            return Repository.GetActedMovies(castOrCrew);
        }

        public IEnumerable<Movie> GetDirectedMovies(CastOrCrew castOrCrew)
        {
            return Repository.GetDirectedMovies(castOrCrew);
        }

         public Dictionary<CastOrCrew, IEnumerable<Movie>> GetCoActors(CastOrCrew actor)
        {
            var coactorDictionary = new Dictionary<CastOrCrew, IEnumerable<Movie>>();
            var actedMovies = GetActedMovies(actor);
            var allActors = Repository.GetAllPeople();

            var coactors = actedMovies.Select(m => m.ActorIds).Aggregate((l1, l2) => 
                        new HashSet<Guid>(l1.Concat(l2)))
                        .Distinct()
                        .Except(new List<Guid> { actor.Id })
                        .Join(allActors, g => g, a => a.Id, (g, a) => a);

            foreach (var coactor in coactors)
            {
                var movies = actedMovies.Where(m => m.ActorIds.Contains(coactor.Id)).Distinct();
                coactorDictionary.Add(coactor, movies);
            }
            return coactorDictionary;
        }

        public string GetJobTitle(CastOrCrew castOrCrew)
        {
            throw new NotImplementedException();
        }

        public bool IsActor(CastOrCrew castOrCrew)
        {
            throw new NotImplementedException();
        }

        public bool IsDirector(CastOrCrew castOrCrew)
        {
            throw new NotImplementedException();
        }

        private CastOrCrewRepository Repository
        {
            get { return CastOrCrewRepository.Instance; }
        }
    }
}
