﻿using MMDBService.MovieDatabase.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMDB.MovieDatabase.Domain.ValueObjects;

namespace MMDBService.MovieDatabase.Services
{
    public class ValidatorService : IValidator
    {
        public bool ValidateProductionYear(ProductionYear productionYear)
        {
            ProductionYear result;
            if (ProductionYear.TryParse(productionYear.Value.ToString(), out result))
                return true;
            else
                return false;
        }
    }
}
