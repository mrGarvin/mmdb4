﻿using MMDBService.MovieDatabase.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Domain.ValueObjects;
using MMDB.MovieDatabase.Repositories;
using MMDB.MovieDatabase.Helper;

namespace MMDBService.MovieDatabase.Services
{

    public class MovieDatabaseService : ICastOrCrewService, IMovieService, ISearchService, IValidator
    {
        private MovieRepository MovieRepo
        {
            get { return MovieRepository.Instance; }
        }

        private CastOrCrewRepository CastOrCrewRepo
        {
            get { return CastOrCrewRepository.Instance; }
        }

        public void AddCastOrCrew(CastOrCrew castOrCrew)
        {
            CastOrCrewRepo.Add(castOrCrew);
        }

        public void AddMovie(Movie movie)
        {
            MovieRepo.AddMovie(movie);
        }

        public CastOrCrew FindCastOrCrewById(Guid id)
        {
            return CastOrCrewRepo.FindBy(id);
        }

        public CastOrCrew FindCastOrCrewByName(string name)
        {
            return CastOrCrewRepo.FindBy(name);
        }

        public Movie FindMovieByTitle(string title)
        {
            return MovieRepo.FindMovieByTitle(title);
        }

        public IEnumerable<Movie> GetActedMovies(CastOrCrew castOrCrew)
        {
            return CastOrCrewRepo.GetActedMovies(castOrCrew);
        }

        public Dictionary<CastOrCrew, IEnumerable<Movie>> GetCoActors(CastOrCrew actor)
        {
            var coactorDictionary = new Dictionary<CastOrCrew, IEnumerable<Movie>>();
            var actedMovies = GetActedMovies(actor);
            var allActors = CastOrCrewRepo.GetAllPeople();

            var coactors = actedMovies.Select(m => m.ActorIds).Aggregate((l1, l2) =>
                        new HashSet<Guid>(l1.Concat(l2)))
                        .Distinct()
                        .Except(new List<Guid> { actor.Id })
                        .Join(allActors, g => g, a => a.Id, (g, a) => a);

            foreach (var coactor in coactors)
            {
                var movies = actedMovies.Where(m => m.ActorIds.Contains(coactor.Id)).Distinct();
                coactorDictionary.Add(coactor, movies);
            }
            return coactorDictionary;
        }

        public IEnumerable<Movie> GetDirectedMovies(CastOrCrew castOrCrew)
        {
            return CastOrCrewRepo.GetDirectedMovies(castOrCrew);
        }

        public bool ValidateProductionYear(ProductionYear productionYear)
        {
            ProductionYear result;
            if (ProductionYear.TryParse(productionYear.Value.ToString(), out result))
                return true;
            else
                return false;
        }

        public string GetJobTitle(CastOrCrew castOrCrew)
        {
            return castOrCrew.JobTitle;
        }

        public bool IsActor(CastOrCrew castOrCrew)
        {
            return castOrCrew.IsActor;
        }

        public bool IsDirector(CastOrCrew castOrCrew)
        {
            return castOrCrew.IsDirector;
        }

        public void AddActorTo(Movie movie, CastOrCrew actor)
        {
            throw new NotImplementedException();
        }

        public void AddDirectorTo(Movie movie, CastOrCrew director)
        {
            throw new NotImplementedException();
        }

        public CastOrCrew[] GetMovieActors(Movie movie)
        {
            Guid[] actorIds = movie.ActorIds.ToArray();
            CastOrCrew[] actorArray = new CastOrCrew[actorIds.Length];

            for (int i = 0; i < actorIds.Length; i++)
            {
                actorArray[i] = CastOrCrewRepo.FindBy(actorIds[i]);
            }

            return actorArray;
        }

        public CastOrCrew[] GetMovieDirectors(Movie movie)
        {
            Guid[] directorIds = movie.DirectorIds.ToArray();
            CastOrCrew[] directorArray = new CastOrCrew[directorIds.Length];

            for (int i = 0; i < directorIds.Length; i++)
            {
                directorArray[i] = CastOrCrewRepo.FindBy(directorIds[i]);
            }

            return directorArray;
        }

        public string[] Search(string freeText, bool ignoreCase)
        {
            IEnumerable<SearchResultItem> movieResult = MovieRepo.GetAllMovies().Where(m => m.Title.Contains(freeText, ignoreCase) ||
                                                     m.ProductionYear.Value.ToString().Contains(freeText, ignoreCase)
                                                     ).Select(m => new MovieSearchResultItem(m));

            IEnumerable<SearchResultItem> castOrCrewResult = CastOrCrewRepo.GetAllPeople().Where(p => p.Name.Contains(freeText, ignoreCase) ||
                                                                            p.DateOfBirth.Year.ToString().Contains(freeText, ignoreCase)
                                                                            ).Select(p => new CastOrCrewSearchResultItem(p));

            SearchResultItem[] resultSearchResultItemArray = movieResult.Concat(castOrCrewResult).ToArray();
            string[] resultStringArray = new string[resultSearchResultItemArray.Length];
            for (int i = 0; i < resultSearchResultItemArray.Length; i++)
            {
                resultStringArray[i] = resultSearchResultItemArray[i].ToString();
            }

            return resultStringArray;
        }
    }
}
