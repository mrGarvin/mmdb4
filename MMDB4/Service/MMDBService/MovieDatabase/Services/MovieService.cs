﻿using System;
using MMDB.MovieDatabase.Domain;
using MMDB.MovieDatabase.Repositories;
using MMDBService.MovieDatabase.Interfaces;

namespace MMDB.MovieDatabase.Services
{
    class MovieService : IMovieService
    {
        public void AddMovie(Movie movie)
        {
            Repository.AddMovie(movie);
        }
        public Movie FindMovieByTitle(string title)
        {
            return Repository.FindMovieByTitle(title);
        }

        public void AddActorTo(Movie movie, CastOrCrew actor)
        {
            throw new NotImplementedException();
        }

        public void AddDirectorTo(Movie movie, CastOrCrew director)
        {
            throw new NotImplementedException();
        }

        public CastOrCrew[] GetMovieActors(Movie movie)
        {
            throw new NotImplementedException();
        }

        public CastOrCrew[] GetMovieDirectors(Movie movie)
        {
            throw new NotImplementedException();
        }

        private MovieRepository Repository
        {
            get { return MovieRepository.Instance; }
        }
    }
}
