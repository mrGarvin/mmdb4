﻿using MMDB.MovieDatabase.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MMDBService.MovieDatabase.Interfaces
{
    [ServiceContract]
    public interface IValidator
    {
        [OperationContract]
        bool ValidateProductionYear(ProductionYear productionYear);
    }
}
