﻿using MMDB.MovieDatabase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MMDBService.MovieDatabase.Interfaces
{
    [ServiceContract]
    public interface ICastOrCrewService
    {
        [OperationContract]
        void AddCastOrCrew(CastOrCrew castOrCrew);

        [OperationContract]
        CastOrCrew FindCastOrCrewById(Guid id);

        [OperationContract]
        CastOrCrew FindCastOrCrewByName(string name);

        [OperationContract]
        IEnumerable<Movie> GetActedMovies(CastOrCrew castOrCrew);

        [OperationContract]
        Dictionary<CastOrCrew, IEnumerable<Movie>> GetCoActors(CastOrCrew actor);

        [OperationContract]
        IEnumerable<Movie> GetDirectedMovies(CastOrCrew castOrCrew);

        [OperationContract]
        string GetJobTitle(CastOrCrew castOrCrew);

        [OperationContract]
        bool IsActor(CastOrCrew castOrCrew);

        [OperationContract]
        bool IsDirector(CastOrCrew castOrCrew);
    }
}
