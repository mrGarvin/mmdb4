﻿using MMDB.MovieDatabase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MMDBService.MovieDatabase.Interfaces
{
    [ServiceContract]
    public interface IMovieService
    {
        [OperationContract]
        void AddActorTo(Movie movie, CastOrCrew actor);

        [OperationContract]
        void AddDirectorTo(Movie movie, CastOrCrew director);

        [OperationContract]
        void AddMovie(Movie movie);

        [OperationContract]
        Movie FindMovieByTitle(string title);

        [OperationContract]
        CastOrCrew[] GetMovieActors(Movie movie);

        [OperationContract]
        CastOrCrew[] GetMovieDirectors(Movie movie);
    }
}
