﻿using MMDB.MovieDatabase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDB.MovieDatabase.Domain.ValueObjects
{
    public class SearchResultItem
    {
        protected SearchResultItem(object item)
        {
            ResultItem = item;
        }

        public object ResultItem { get; }
        public virtual string Type { get { return ResultItem.GetType().ToString(); } }
    }

    class MovieSearchResultItem : SearchResultItem
    {
        public MovieSearchResultItem(Movie movie)
            :base(movie)
        {
            
        }

        public override string Type
        {
            get
            {
                return "Movie";
            }
        }

        public override string ToString()
        {
            return $"[{Type}]: {((Movie)ResultItem).Title} ({((Movie)ResultItem).ProductionYear.Value})";
        }
    }

    class CastOrCrewSearchResultItem : SearchResultItem
    {
        public CastOrCrewSearchResultItem(CastOrCrew castOrCrew)
            : base(castOrCrew)
        {
        }

        public override string Type
        {
            get
            {
                return ((CastOrCrew)ResultItem).JobTitle;
            }
        }

        public override string ToString()
        {
            return $"[{Type}]: {((CastOrCrew)ResultItem).Name} ({((CastOrCrew)ResultItem).DateOfBirth:yyy-MM-dd})";
        }
    }
}
