﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDB.MovieDatabase.Domain.ValueObjects
{
    interface IValueObject
    {
        bool Validate();
    }
}
